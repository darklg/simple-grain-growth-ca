/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graingrowth;

/**
 *
 * @author darglk
 */
public class Cell {
    private int id;
    private int xCoord;
    private int yCoord;
    private int width;
    private int height;
    private int colX;
    private int colY;
    private boolean canChangeId;
    public Cell(int i, int x, int y, int w, int h, int cx, int cy){
        this.id = i;
        this.height = h;
        this.width = w;
        this.xCoord = x;
        this.yCoord = y;
        this.canChangeId = true;
        this.colX = cx;
        this.colY = cy;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        if(this.canChangeId)
            this.id = id;
    }

    /**
     * @return the xCoord
     */
    public int getxCoord() {
        return xCoord;
    }

    /**
     * @param xCoord the xCoord to set
     */
    public void setxCoord(int xCoord) {
        this.xCoord = xCoord;
    }

    /**
     * @return the yCoord
     */
    public int getyCoord() {
        return yCoord;
    }

    /**
     * @param yCoord the yCoord to set
     */
    public void setyCoord(int yCoord) {
        this.yCoord = yCoord;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return the canChangeId
     */
    public boolean isCanChangeId() {
        return canChangeId;
    }

    /**
     * @param canChangeId the canChangeId to set
     */
    public void setCanChangeId(boolean canChangeId) {
        this.canChangeId = canChangeId;
    }

    /**
     * @return the colX
     */
    public int getColX() {
        return colX;
    }

    /**
     * @param colX the colX to set
     */
    public void setColX(int colX) {
        this.colX = colX;
    }

    /**
     * @return the colY
     */
    public int getColY() {
        return colY;
    }

    /**
     * @param colY the colY to set
     */
    public void setColY(int colY) {
        this.colY = colY;
    }
    
    
}
