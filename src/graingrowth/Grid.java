/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graingrowth;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author darglk
 */
public class Grid {
    private Cell[][] cells;
    private int width;
    private int height;
    private int grainsNum;
    
    private ArrayList<Cell> fillRandomlyCellsId(){
        ArrayList<Cell> cellsCollection = new ArrayList<>();
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                cells[i][j].setId(this.randInt(1, grainsNum + 1));
                cellsCollection.add(cells[i][j]);
            }
        }
        return cellsCollection;
    }
    
    private ArrayList<Cell> fillCellsToCollection(){
        ArrayList<Cell> cellsCollection = new ArrayList<>();
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){                
                cellsCollection.add(cells[i][j]);
            }
        }
        return cellsCollection;
    }
    
    private int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min)) + min;
        return randomNum;
    }   
    
    public void changeCellsId(Graphics g,int id, int newId){
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                if(this.cells[i][j].getId() == id){
                    this.cells[i][j].setId(newId);
                    this.cells[i][j].setCanChangeId(false);
                    DrawComponent.repaintCell(g, i, j, cells, grainsNum);
                }
            }
        }
    }
    
    public void lockCellsId(int id){
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                if(this.cells[i][j].getId() == id){
                    this.cells[i][j].setCanChangeId(false);
                }
            }
        }
    }
    
    public void resetUnselectedCells(){
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){               
                this.cells[i][j].setId(0);                
            }
        }
    }
    
    public boolean checkForZeros(){
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                if(cells[i][j].getId() == 0){
                    return false;
                }
            }
        }
        return true;
    }        
    
    //private methods for grain boundary shape    
    private int gbCheckTop(int x, int y, boolean boundaryConditions){
        int retId = 0;
        if(((x - 1) >= 0) && (cells[x - 1][y].getId() > 0) && (cells[x - 1][y].isCanChangeId())){
                retId = cells[x - 1][y].getId();
            }else if(boundaryConditions && (cells[getHeight() - 1][y].getId() > 0) && (x - 1 < 0) && (cells[getHeight() - 1][y].isCanChangeId())){
                retId = cells[getHeight() - 1][y].getId();
            }
        return retId;
    }
    
    private int gbCheckRightTop(int x, int y, boolean boundaryConditions){
        int retId = 0;
        if((x - 1 >= 0) && (y + 1 < width) && (cells[x - 1][y + 1].getId() > 0) && (cells[x - 1][y + 1].isCanChangeId())){
                retId = cells[x - 1][y + 1].getId();
            }else if(boundaryConditions && (cells[height - 1][0].getId() > 0) && (y + 1 >= width) && (x - 1 < 0) && (cells[height - 1][0].isCanChangeId())){
                retId = cells[height - 1][0].getId();
            }
        return retId;
    }
    
    private int gbCheckRight(int x, int y, boolean boundaryConditions){
        int retId = 0;
        if(((y + 1) < width) && (cells[x][y + 1].getId() > 0) && (cells[x][y + 1].isCanChangeId())){
                retId = cells[x][y + 1].getId();
            }else if(boundaryConditions && (cells[x][0].getId() > 0) && (y + 1 >= getWidth()) && (cells[x][0].isCanChangeId())){                
                retId = cells[x][0].getId();
            }
        return retId;
    }
    
    private int gbCheckRightDown(int x, int y, boolean boundaryConditions){
        int retId = 0;
        if((x + 1 < height) && (y + 1 < width) && (cells[x + 1][y + 1].getId() > 0) && (cells[x + 1][y + 1].isCanChangeId())){//ok
                retId = cells[x + 1][y + 1].getId();
            }else if(boundaryConditions && (cells[0][0].getId() > 0) && (y + 1 >= width) && (x + 1 >= height) && (cells[0][0].isCanChangeId())){
                retId = cells[0][0].getId();
            }
        return retId;
    }
    
    private int gbCheckDown(int x, int y, boolean boundaryConditions){
        int retId = 0;
        if(((x + 1) < height) && (cells[x + 1][y].getId() > 0) && (cells[x + 1][y].isCanChangeId())){
                retId = cells[x + 1][y].getId();
            }else if(boundaryConditions && (cells[0][y].getId() > 0) && (x + 1 >= getHeight()) && (cells[0][y].isCanChangeId())){
                retId = cells[0][y].getId();    
            }
        return retId;
    }
    
    private int gbCheckLeftDown(int x, int y, boolean boundaryConditions){
        int retId = 0;
        if((x + 1 < height) && (y - 1 >= 0) && (cells[x + 1][y - 1].getId() > 0) && (cells[x + 1][y - 1].isCanChangeId())){
                retId = cells[x + 1][y - 1].getId();
            }else if(boundaryConditions && (cells[getHeight() - 1][0].getId() > 0) && (y - 1 < 0) && (x + 1) >= getHeight() && (cells[getHeight() - 1][0].isCanChangeId())){                
                retId = cells[getHeight() - 1][0].getId();
            }
        return retId;
    }
    
    private int gbCheckLeft(int x, int y, boolean boundaryConditions){
        int retId = 0;        
        if(((y - 1) >= 0) && (cells[x][y - 1].getId() > 0)&& (cells[x][y - 1].isCanChangeId())){
                retId = cells[x][y - 1].getId();
            }else if(boundaryConditions && (cells[x][getWidth() - 1].getId() > 0) && (y - 1 < 0)&& (cells[x][getWidth() - 1].isCanChangeId())){
                retId = cells[x][getWidth() - 1].getId();
            }
        return retId;
    }
    
    private int gbCheckLeftTop(int x, int y, boolean boundaryConditions){
        int retId = 0;
        if((x - 1 >= 0) && (y - 1 >= 0) && (cells[x - 1][y - 1].getId() > 0) && (cells[x - 1][y - 1].isCanChangeId())){//ok
                retId = cells[x - 1][y - 1].getId();
            }else if(boundaryConditions && (cells[height - 1][width - 1].getId() > 0) && (y - 1 < 0) && (x - 1 < 0) && (cells[height - 1][width - 1].isCanChangeId())){
                retId = cells[height - 1][width - 1].getId();
            }
        return retId;
    }
    
    //rule 1 helpers
    private boolean checkRule1_1(int x, int y, boolean boundaryConditions){
        int a = this.gbCheckRightTop(x, y, boundaryConditions);
        int b = this.gbCheckTop(x, y, boundaryConditions);
        int c = this.gbCheckLeftTop(x, y, boundaryConditions);
        int d = this.gbCheckLeft(x, y, boundaryConditions);
        int e = this.gbCheckLeftDown(x, y, boundaryConditions);
        boolean res1 = (a == b);
        boolean res2 = (a == c);
        boolean res3 = (a == d);
        boolean res4 = (a == e);
        return res1 == res2 == res3 == res4;
    }
    
    private boolean checkRule1_2(int x, int y, boolean boundaryConditions){
        int a = this.gbCheckRightTop(x, y, boundaryConditions);
        int b = this.gbCheckTop(x, y, boundaryConditions);
        int c = this.gbCheckLeftTop(x, y, boundaryConditions);
        int d = this.gbCheckRight(x, y, boundaryConditions);
        int e = this.gbCheckRightDown(x, y, boundaryConditions);
        boolean res1 = (a == b);
        boolean res2 = (a == c);
        boolean res3 = (a == d);
        boolean res4 = (a == e);
        return res1 == res2 == res3 == res4;
    }
    
    private boolean checkRule1_3(int x, int y, boolean boundaryConditions){
        int a = this.gbCheckRightTop(x, y, boundaryConditions);
        int b = this.gbCheckRight(x, y, boundaryConditions);
        int c = this.gbCheckRightDown(x, y, boundaryConditions);
        int d = this.gbCheckDown(x, y, boundaryConditions);
        int e = this.gbCheckLeftDown(x, y, boundaryConditions);
        boolean res1 = (a == b);
        boolean res2 = (a == c);
        boolean res3 = (a == d);
        boolean res4 = (a == e);
        return res1 == res2 == res3 == res4;
    }
    
    private boolean checkRule1_4(int x, int y, boolean boundaryConditions){
        int a = this.gbCheckRightDown(x, y, boundaryConditions);
        int b = this.gbCheckDown(x, y, boundaryConditions);
        int c = this.gbCheckLeftDown(x, y, boundaryConditions);
        int d = this.gbCheckLeft(x, y, boundaryConditions);
        int e = this.gbCheckLeftTop(x, y, boundaryConditions);
        boolean res1 = (a == b);
        boolean res2 = (a == c);
        boolean res3 = (a == d);
        boolean res4 = (a == e);
        return res1 == res2 == res3 == res4;
    }
    
    private boolean checkRule1_5(int x, int y, boolean boundaryConditions){
        int a = this.gbCheckRightTop(x, y, boundaryConditions);
        int b = this.gbCheckTop(x, y, boundaryConditions);
        int c = this.gbCheckLeftTop(x, y, boundaryConditions);
        int d = this.gbCheckLeft(x, y, boundaryConditions);
        int e = this.gbCheckRight(x, y, boundaryConditions);
        boolean res1 = (a == b);
        boolean res2 = (a == c);
        boolean res3 = (a == d);
        boolean res4 = (a == e);
        return res1 == res2 == res3 == res4;
    }
    
    private boolean checkRule1_6(int x, int y, boolean boundaryConditions){
        int a = this.gbCheckRightDown(x, y, boundaryConditions);
        int b = this.gbCheckDown(x, y, boundaryConditions);
        int c = this.gbCheckLeftDown(x, y, boundaryConditions);
        int d = this.gbCheckLeft(x, y, boundaryConditions);
        int e = this.gbCheckRight(x, y, boundaryConditions);
        boolean res1 = (a == b);
        boolean res2 = (a == c);
        boolean res3 = (a == d);
        boolean res4 = (a == e);
        return res1 == res2 == res3 == res4;
    }
    
    private boolean checkRule1_7(int x, int y, boolean boundaryConditions){
        int a = this.gbCheckLeftTop(x, y, boundaryConditions);
        int b = this.gbCheckTop(x, y, boundaryConditions);
        int c = this.gbCheckLeft(x, y, boundaryConditions);
        int d = this.gbCheckLeftDown(x, y, boundaryConditions);
        int e = this.gbCheckDown(x, y, boundaryConditions);
        boolean res1 = (a == b);
        boolean res2 = (a == c);
        boolean res3 = (a == d);
        boolean res4 = (a == e);
        return res1 == res2 == res3 == res4;
    }
    
    private boolean checkRule1_8(int x, int y, boolean boundaryConditions){
        int a = this.gbCheckRightTop(x, y, boundaryConditions);
        int b = this.gbCheckTop(x, y, boundaryConditions);
        int c = this.gbCheckRight(x, y, boundaryConditions);
        int d = this.gbCheckRightDown(x, y, boundaryConditions);
        int e = this.gbCheckDown(x, y, boundaryConditions);
        boolean res1 = (a == b);
        boolean res2 = (a == c);
        boolean res3 = (a == d);
        boolean res4 = (a == e);
        return res1 == res2 == res3 == res4;
    }
    
    private boolean rule1(Cell[][] cellsCopy, int x, int y, boolean boundaryConditions){
        Cell cell = cells[x][y];
        if(cell.getId() == 0){
            if(this.checkRule1_1(x, y, boundaryConditions) && (this.gbCheckTop(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckTop(x, y, boundaryConditions)); return true;}
            if(this.checkRule1_2(x, y, boundaryConditions) && (this.gbCheckTop(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckTop(x, y, boundaryConditions)); return true;}
            if(this.checkRule1_3(x, y, boundaryConditions) && (this.gbCheckRight(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckRight(x, y, boundaryConditions)); return true;}
            if(this.checkRule1_4(x, y, boundaryConditions) && (this.gbCheckLeft(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckLeft(x, y, boundaryConditions)); return true;}
            if(this.checkRule1_5(x, y, boundaryConditions) && (this.gbCheckLeft(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckLeft(x, y, boundaryConditions)); return true;}
            if(this.checkRule1_6(x, y, boundaryConditions) && (this.gbCheckLeft(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckLeft(x, y, boundaryConditions)); return true;}
            if(this.checkRule1_7(x, y, boundaryConditions) && (this.gbCheckLeft(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckLeft(x, y, boundaryConditions)); return true;}
            if(this.checkRule1_8(x, y, boundaryConditions) && (this.gbCheckRight(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckRight(x, y, boundaryConditions)); return true;}
        }
        return false;
    }
    //rule 2 helpers
    private boolean checkRule2_1(int x, int y, boolean boundaryConditions){
        int rightId = this.gbCheckRight(x, y, boundaryConditions);
        int topId = this.gbCheckTop(x, y, boundaryConditions);
        int downId = this.gbCheckDown(x, y, boundaryConditions);
        boolean sub_result = (rightId == topId);
        boolean result = (rightId == downId);
        return result == sub_result;
    }
    
    private boolean checkRule2_2(int x, int y, boolean boundaryConditions){
        int rightId = this.gbCheckRight(x, y, boundaryConditions);
        int topId = this.gbCheckTop(x, y, boundaryConditions);
        int leftId = this.gbCheckLeft(x, y, boundaryConditions);
        boolean sub_result = (rightId == topId);
        boolean result = (rightId == leftId);
        return result == sub_result;
    }
    
    private boolean checkRule2_3(int x, int y, boolean boundaryConditions){
        int leftId = this.gbCheckLeft(x, y, boundaryConditions);
        int topId = this.gbCheckTop(x, y, boundaryConditions);
        int downId = this.gbCheckDown(x, y, boundaryConditions);
        boolean sub_result = (leftId == topId);
        boolean result = (leftId == downId);
        return result == sub_result;
    }
    
    private boolean checkRule2_4(int x, int y, boolean boundaryConditions){
        int rightId = this.gbCheckRight(x, y, boundaryConditions);
        int leftId = this.gbCheckLeft(x, y, boundaryConditions);
        int downId = this.gbCheckDown(x, y, boundaryConditions);
        boolean sub_result = (rightId == leftId);
        boolean result = (rightId == downId);
        return result == sub_result;
    }
    
    private boolean rule2(Cell[][] cellsCopy, int x, int y, boolean boundaryConditions){
        Cell cell = cells[x][y];
        if(cell.getId() == 0){
            if(this.checkRule2_1(x, y, boundaryConditions) && (this.gbCheckRight(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckRight(x, y, boundaryConditions)); return true;};
            if(this.checkRule2_2(x, y, boundaryConditions) && (this.gbCheckRight(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckRight(x, y, boundaryConditions)); return true;};
            if(this.checkRule2_3(x, y, boundaryConditions) && (this.gbCheckLeft(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckLeft(x, y, boundaryConditions)); return true;};
            if(this.checkRule2_4(x, y, boundaryConditions) && (this.gbCheckLeft(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckLeft(x, y, boundaryConditions)); return true;};
        }
        return false;
    }
    
    //Rule 3 helpers
    private boolean checkRule3_1(int x, int y, boolean boundaryConditions){
        int leftTopId = this.gbCheckLeftTop(x, y, boundaryConditions);
        int leftDownId = this.gbCheckLeftDown(x, y, boundaryConditions);
        int rightTopId = this.gbCheckRightTop(x, y, boundaryConditions);
        boolean sub_result = (leftTopId == leftDownId);
        boolean result = (leftTopId == rightTopId);
        return result == sub_result;
    }
    
    private boolean checkRule3_2(int x, int y, boolean boundaryConditions){
        int leftTopId = this.gbCheckLeftTop(x, y, boundaryConditions);
        int rightDownId = this.gbCheckRightDown(x, y, boundaryConditions);
        int rightTopId = this.gbCheckRightTop(x, y, boundaryConditions);
        boolean sub_result = (leftTopId == rightDownId);
        boolean result = (leftTopId == rightTopId);
        return result == sub_result;
    }
    
    private boolean checkRule3_3(int x, int y, boolean boundaryConditions){
        int leftDownId = this.gbCheckLeftDown(x, y, boundaryConditions);
        int rightDownId = this.gbCheckRightDown(x, y, boundaryConditions);
        int rightTopId = this.gbCheckRightTop(x, y, boundaryConditions);
        boolean sub_result = (leftDownId == rightDownId);
        boolean result = (leftDownId == rightTopId);
        return result == sub_result;
    }
    
    private boolean checkRule3_4(int x, int y, boolean boundaryConditions){
        int leftTopId = this.gbCheckLeftTop(x, y, boundaryConditions);
        int leftDownId = this.gbCheckLeftDown(x, y, boundaryConditions);
        int rightDownId = this.gbCheckRightDown(x, y, boundaryConditions);
        boolean sub_result = (leftTopId == leftDownId);
        boolean result = (leftTopId == rightDownId);
        return result == sub_result;
    }
    
    private boolean rule3(Cell[][] cellsCopy, int x, int y, boolean boundaryConditions){
        Cell cell = cells[x][y];
        if(cell.getId() == 0){
            if(this.checkRule3_1(x, y, boundaryConditions) && (this.gbCheckRightTop(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckRightTop(x, y, boundaryConditions)); return true;};
            if(this.checkRule3_2(x, y, boundaryConditions) && (this.gbCheckRightTop(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckRightTop(x, y, boundaryConditions)); return true;};
            if(this.checkRule3_3(x, y, boundaryConditions) && (this.gbCheckRightTop(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckRightTop(x, y, boundaryConditions)); return true;};
            if(this.checkRule3_4(x, y, boundaryConditions) && (this.gbCheckLeftTop(x, y, boundaryConditions) > 0)) {cellsCopy[x][y].setId(this.gbCheckLeftTop(x, y, boundaryConditions)); return true;};
        }
        return false;
    }
    
    //Rule 4 helpers
    private void checkForKey(Map<Integer, Integer> neighbors, Integer id){
        Integer value = neighbors.get(id);
        if (value != null && (id > 0)) {
           neighbors.put(id, neighbors.get(id) + 1);
        }else if( id > 0) {
           neighbors.put(id,1);
        }
    }
           
    private Map<Integer, Integer> rule4_countNeighbors(int x, int y, boolean boundaryConditions){
        //  cell ID: count:
        Map<Integer, Integer> neighbors = new HashMap<>();
        this.checkForKey(neighbors, this.gbCheckDown(x, y, boundaryConditions));
        this.checkForKey(neighbors, this.gbCheckLeftDown(x, y, boundaryConditions));
        this.checkForKey(neighbors, this.gbCheckRightDown(x, y, boundaryConditions));
        this.checkForKey(neighbors, this.gbCheckLeft(x, y, boundaryConditions));
        this.checkForKey(neighbors, this.gbCheckRight(x, y, boundaryConditions));
        this.checkForKey(neighbors, this.gbCheckLeftTop(x, y, boundaryConditions));
        this.checkForKey(neighbors, this.gbCheckTop(x, y, boundaryConditions));
        this.checkForKey(neighbors, this.gbCheckRightTop(x, y, boundaryConditions));
        
        return neighbors;
    }
    
    private Map.Entry<Integer, Integer> checkMaxEntry(Map<Integer, Integer> neighbors){
        Map.Entry<Integer, Integer> maxEntry = null;

            for (Map.Entry<Integer, Integer> entry : neighbors.entrySet())
            {
                maxEntry = entry;                
                if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
                {
                    maxEntry = entry;
                }
            }
            return maxEntry;
    }
    
    private void rule4(Cell[][] cellsCopy, int x, int y, boolean boundaryConditions, int probability){
        Cell cell = cells[x][y];
        if((cell.getId() == 0) && (this.randInt(1,100) <= probability)){
            Map<Integer, Integer> neighbors = this.rule4_countNeighbors(x, y, boundaryConditions);
            Map.Entry<Integer, Integer> maxEntry = null;

            for (Map.Entry<Integer, Integer> entry : neighbors.entrySet())
            {
                maxEntry = entry;                
                if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
                {
                    maxEntry = entry;
                }
            }
            if(maxEntry != null)
                cellsCopy[x][y].setId(maxEntry.getKey());
        }
    }
    
    private void copyCells(Cell[][] cellCopy){
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
               if(cellCopy[i][j].getId() != 0)
                    cells[i][j].setId(cellCopy[i][j].getId()); 
            }
        }
    }
    
    private void mooreNeighborhood(Cell[][] cellCopy, int x, int y, Graphics g, boolean boundaryConditions){
        Cell cell = this.cells[x][y];
        if(cell.getId() == 0){
            Map<Integer, Integer> neighbors = new HashMap<>();
            this.checkForKey(neighbors, this.gbCheckTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRight(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeft(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRightTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRightDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeftDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeftTop(x, y, boundaryConditions));
            Map.Entry<Integer, Integer> maxEntry = this.checkMaxEntry(neighbors);
            if(maxEntry != null)
                cellCopy[x][y].setId(maxEntry.getKey());
        }
    }
    
    private void hexagonal1(Cell[][] cellCopy, int x, int y, Graphics g, boolean boundaryConditions){
        Cell cell = this.cells[x][y];
        if(cell.getId() == 0){
            Map<Integer, Integer> neighbors = new HashMap<>();
            this.checkForKey(neighbors, this.gbCheckTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRight(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeft(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeftDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRightTop(x, y, boundaryConditions));
            Map.Entry<Integer, Integer> maxEntry = this.checkMaxEntry(neighbors);
            if(maxEntry != null)
                cellCopy[x][y].setId(maxEntry.getKey());
        }
    }
    
    private void hexagonal2(Cell[][] cellCopy, int x, int y, Graphics g, boolean boundaryConditions){
        Cell cell = this.cells[x][y];
        if(cell.getId() == 0){
            Map<Integer, Integer> neighbors = new HashMap<>();
            this.checkForKey(neighbors, this.gbCheckTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRight(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeft(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeftTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRightDown(x, y, boundaryConditions));
            Map.Entry<Integer, Integer> maxEntry = this.checkMaxEntry(neighbors);
            if(maxEntry != null)
                cellCopy[x][y].setId(maxEntry.getKey());
        }
    }
    
    private void hexagonalRandom(Cell[][] cellCopy, int x, int y, Graphics g, boolean boundaryConditions){
        Random r = new Random();
        int res = r.nextInt(2 - 1 + 1) + 1;
        switch(res){
            case 1:
                this.hexagonal1(cellCopy, x, y, g, boundaryConditions);
            break;
            case 2:
                this.hexagonal2(cellCopy, x, y, g, boundaryConditions);
            break;
        }
    }
    
    private void pentagonal1(Cell[][] cellCopy, int x, int y, Graphics g, boolean boundaryConditions){
        Cell cell = this.cells[x][y];
        if(cell.getId() == 0){
            Map<Integer, Integer> neighbors = new HashMap<>();
            this.checkForKey(neighbors, this.gbCheckTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRightTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRight(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRightDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckDown(x, y, boundaryConditions));
            Map.Entry<Integer, Integer> maxEntry = this.checkMaxEntry(neighbors);
            if(maxEntry != null)
                cellCopy[x][y].setId(maxEntry.getKey());
        }
    }
    
    private void pentagonal2(Cell[][] cellCopy, int x, int y, Graphics g, boolean boundaryConditions){
        Cell cell = this.cells[x][y];
        if(cell.getId() == 0){
            Map<Integer, Integer> neighbors = new HashMap<>();
            this.checkForKey(neighbors, this.gbCheckTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeftTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeft(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeftDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckDown(x, y, boundaryConditions));
            Map.Entry<Integer, Integer> maxEntry = this.checkMaxEntry(neighbors);
            if(maxEntry != null)
                cellCopy[x][y].setId(maxEntry.getKey());
        }
    }
    
    private void pentagonal3(Cell[][] cellCopy, int x, int y, Graphics g, boolean boundaryConditions){
        Cell cell = this.cells[x][y];
        if(cell.getId() == 0){
            Map<Integer, Integer> neighbors = new HashMap<>();
            this.checkForKey(neighbors, this.gbCheckLeft(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeftDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRightDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRight(x, y, boundaryConditions));
            Map.Entry<Integer, Integer> maxEntry = this.checkMaxEntry(neighbors);
            if(maxEntry != null)
                cellCopy[x][y].setId(maxEntry.getKey());
        }
    }
    
    private void pentagonal4(Cell[][] cellCopy, int x, int y, Graphics g, boolean boundaryConditions){
        Cell cell = this.cells[x][y];
        if(cell.getId() == 0){
            Map<Integer, Integer> neighbors = new HashMap<>();
            this.checkForKey(neighbors, this.gbCheckLeft(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeftTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRightTop(x, y, boundaryConditions));            
            this.checkForKey(neighbors, this.gbCheckRight(x, y, boundaryConditions));
            Map.Entry<Integer, Integer> maxEntry = this.checkMaxEntry(neighbors);
            if(maxEntry != null)
                cellCopy[x][y].setId(maxEntry.getKey());
        }
    }
    
    private void pentagonalRandom(Cell[][] cellCopy, int x, int y, Graphics g, boolean boundaryConditions){
        Random r = new Random();
        int res = r.nextInt(4 - 1 + 1) + 1;
        switch(res){
            case 1:
                this.pentagonal1(cellCopy, x, y, g, boundaryConditions);
            break;
            case 2:
                this.pentagonal2(cellCopy, x, y, g, boundaryConditions);
            break;
            case 3:
                this.pentagonal3(cellCopy, x, y, g, boundaryConditions);
            break;
            case 4:
                this.pentagonal4(cellCopy, x, y, g, boundaryConditions);
            break;
        }
    }
    
    private void vonNeumannNeighborhood(Cell[][] cellCopy, int x, int y, Graphics g, boolean boundaryConditions){
        Cell cell = this.cells[x][y];
        if(cell.getId() == 0){
            Map<Integer, Integer> neighbors = new HashMap<>();
            this.checkForKey(neighbors, this.gbCheckDown(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckLeft(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckTop(x, y, boundaryConditions));
            this.checkForKey(neighbors, this.gbCheckRight(x, y, boundaryConditions));
            Map.Entry<Integer, Integer> maxEntry = this.checkMaxEntry(neighbors);
            if(maxEntry != null)
                cellCopy[x][y].setId(maxEntry.getKey());
        }
    }
    
    public Grid(int width, int height){
        this.width = width;
        this.height = height;
        this.cells = new Cell[height][width];
        this.grainsNum = 0;
    }
    
    //mc helpers
    
    private Map<Integer, Integer> vonNeumann_countNeighbors(int x, int y, boolean boundaryConditions){
        //  cell ID: count:
        Map<Integer, Integer> neighbors = new HashMap<>();
        this.checkForKey(neighbors, this.gbCheckDown(x, y, boundaryConditions));                
        this.checkForKey(neighbors, this.gbCheckLeft(x, y, boundaryConditions));
        this.checkForKey(neighbors, this.gbCheckRight(x, y, boundaryConditions));        
        this.checkForKey(neighbors, this.gbCheckTop(x, y, boundaryConditions));        
        
        return neighbors;
    }
    
    private int countNeighbors(Cell cell, boolean boundaryConditions, String neighborhoodType){
        int counter = 0;
        if(this.gbCheckDown(cell.getColX(), cell.getColY(), boundaryConditions)!=cell.getId())counter++;
        if(this.gbCheckLeft(cell.getColX(), cell.getColY(), boundaryConditions)!=cell.getId())counter++;
        if(this.gbCheckTop(cell.getColX(), cell.getColY(), boundaryConditions)!=cell.getId())counter++;
        if(this.gbCheckRight(cell.getColX(), cell.getColY(), boundaryConditions)!=cell.getId())counter++;
        if("Moore".equals(neighborhoodType)){
            if(this.gbCheckLeftTop(cell.getColX(), cell.getColY(), boundaryConditions)!=cell.getId())counter++;
            if(this.gbCheckLeftDown(cell.getColX(), cell.getColY(), boundaryConditions)!=cell.getId())counter++;
            if(this.gbCheckRightTop(cell.getColX(), cell.getColY(), boundaryConditions)!=cell.getId())counter++;
            if(this.gbCheckRightDown(cell.getColX(), cell.getColY(), boundaryConditions)!=cell.getId())counter++;
        }
        return counter;
    }
    
    public void monteCarloGrainGrowth(Graphics g, boolean boundaryConditions, double kbT, int mcs, double J, String neighborhoodType){
        ArrayList<Cell> cellsCollection = this.fillRandomlyCellsId();
        for(int i = 0; i < mcs; i++){
            
            while(!cellsCollection.isEmpty()){
                Cell cell = cellsCollection.remove(this.randInt(0, cellsCollection.size()));
                Map<Integer, Integer> neighbors = null;
                neighbors = this.vonNeumann_countNeighbors(cell.getColX(), cell.getColY(), boundaryConditions);
                int cellIdOcurrences =this.countNeighbors(cell, boundaryConditions,neighborhoodType);
                int oldId = cell.getId();
                ArrayList<Object> neighborsList = new ArrayList<> (Arrays.asList( neighbors.keySet().toArray()));
                if(neighborsList.size() > 0){
                    Integer randCell = (Integer)neighborsList.get(this.randInt(0,neighborsList.size()));
                    int newId = randCell;//this.randInt(1, this.grainsNum + 1);
                    double Ebefore = J * cellIdOcurrences;
                    cell.setId(newId);
                
                
                cellIdOcurrences = this.countNeighbors(cell, boundaryConditions,neighborhoodType);
                double Eafter = J * cellIdOcurrences;
                cell.setId(oldId);
                double deltaE = Eafter - Ebefore;
                double probability = Math.exp(-deltaE / kbT);
                if(deltaE > 0){
                    if(Math.random() <= probability){
                        cell.setId(newId);
                    }
                }else{
                    cell.setId(newId);
                }
            }}
            cellsCollection = this.fillCellsToCollection();
        }
    }
    
    public void gbGrainGrowth(Cell [][] cellCopy, Graphics g, boolean boundaryConditions, int probability){
        while(!this.checkForZeros()){
            for(int i = 0; i < height; i++){
                for(int j = 0; j < width; j++){
                    if(!this.rule1(cellCopy, i, j, boundaryConditions)){
                        if(!this.rule2(cellCopy, i, j, boundaryConditions)){
                            if(!this.rule3(cellCopy, i, j, boundaryConditions)){
                                this.rule4(cellCopy, i, j, boundaryConditions, probability);
                            }
                        }
                    }
                }
            }
            this.copyCells(cellCopy);
            //this.printCellIds();
            this.clearCellCopy(cellCopy);
        }
    }
    
    public void grainGrowth(Cell [][] cellCopy, Graphics g, String neighborhoodType, boolean boundaryConditions){
        Thread thread = new Thread(new Runnable(){
            public void run(){
         
            while(!Grid.this.checkForZeros()){
                for(int i = 0; i < height; i++){
                    for(int j = 0; j < width; j++){
                        if(cells[i][j].isCanChangeId()){
                            switch(neighborhoodType){
                                case "Von Neumann":
                                    Grid.this.vonNeumannNeighborhood(cellCopy,i, j, g, boundaryConditions);
                                    DrawComponent.repaintCell(g, i, j, cells, Grid.this.grainsNum);
                                break;
                                case "Moore":
                                    Grid.this.mooreNeighborhood(cellCopy, i, j, g, boundaryConditions);
                                    DrawComponent.repaintCell(g, i, j, cells, Grid.this.grainsNum);
                                break;
                                case "Pentagonal":
                                    Grid.this.pentagonalRandom(cellCopy, i, j, g, boundaryConditions);
                                    DrawComponent.repaintCell(g, i, j, cells, Grid.this.grainsNum);
                                break;
                                case "Hexagonal":
                                    Grid.this.hexagonalRandom(cellCopy, i, j, g, boundaryConditions);
                                    DrawComponent.repaintCell(g, i, j, cells, Grid.this.grainsNum);
                                break;    
                            }
                        }
                    }
                }
                Grid.this.copyCells(cellCopy);
                try {
                    //this.printCellIds();
                    Thread.currentThread().sleep(10);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Grid.class.getName()).log(Level.SEVERE, null, ex);
                }
                Grid.this.clearCellCopy(cellCopy);
            }
            }
        });
        thread.start();
    }
    
    public void clearCellCopy(Cell[][] cellCopy){
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                cellCopy[i][j].setId(0);
            }
        }
    }
    
    //inclusions helpers
    private boolean checkTopGrainBoundary(Cell cell, int x, int y){        
        return ((x - 1) >= 0) && (cell.getId() != cells[x - 1][y].getId());
    }
    
    private boolean checkRightTopGrainBoundary(Cell cell, int x, int y){        
        return (x - 1 >= 0) && ((y + 1) < (width - 1)) && (cell.getId() != cells[x - 1][y + 1].getId());
    }
    
    private boolean checkRightGrainBoundary(Cell cell, int x, int y){        
        return ((y + 1) < (width - 1)) && (cell.getId() != cells[x][y + 1].getId());
    }
    
    private boolean checkRightBottomGrainBoundary(Cell cell, int x, int y){        
        return ((x + 1) < (height - 1)) && ((y + 1) < (width - 1)) && (cell.getId() != cells[x + 1][y + 1].getId());
    }
    
    private boolean checkBottomGrainBoundary(Cell cell, int x, int y){        
        return ((x + 1) < (height - 1)) && (cell.getId() != cells[x + 1][y].getId());
    }
    
    private boolean checkLeftBottomGrainBoundary(Cell cell, int x, int y){        
        return ((x + 1) < (height - 1)) && ((y - 1) >= 0) && (cell.getId() != cells[x + 1][y - 1].getId());
    }
    
    private boolean checkLeftGrainBoundary(Cell cell, int x, int y){        
        return ((y - 1) >= 0) && (cell.getId() != cells[x][y - 1].getId());
    }
    
    private boolean checkLeftTopGrainBoundary(Cell cell, int x, int y){        
        return ((y - 1) >= 0) && ((x - 1) >= 0) && (cell.getId() != cells[x - 1][y - 1].getId());
    }
    
    private boolean checkGrainBoundaries(Cell cell, int x, int y){
        return this.checkBottomGrainBoundary(cell, x, y) || this.checkLeftBottomGrainBoundary(cell, x, y) || this.checkLeftGrainBoundary(cell, x, y)||
                this.checkLeftTopGrainBoundary(cell, x, y) || this.checkTopGrainBoundary(cell, x, y) || this.checkRightTopGrainBoundary(cell, x, y) || this.checkRightGrainBoundary(cell, x, y) ||
                this.checkRightBottomGrainBoundary(cell, x, y);
    }
    
    //inclusions helpers
    private boolean canAddCircularInclusion(int radius){
        return false;
    }
    
    private boolean canAddSquareInclusion(int diagonal, int x, int y){        
        for(int i = 0; i < diagonal; i++){
            for(int j = 0; j < diagonal; j++){
                if(x + i < height && y + j < width){
                   if(cells[x + i][y + j].getId() != 0) 
                       return false;
                }
            }
        }
        return true;
    }
    
    public void addCircularInclusions(Graphics g, int numOfInclusions, int radius){
        for(int i = 0; i < numOfInclusions; i++){
            Random r = new Random();
            int res_x = r.nextInt(height - 1);
            int res_y = r.nextInt(width - 1);
            if(this.checkForZeros()){
                while(!this.checkGrainBoundaries(cells[res_x][res_y], res_x, res_y)){
                    res_x = r.nextInt(height - 1);
                    res_y = r.nextInt(width - 1);
                }
            }
            cells[res_x][res_y].setId(-1);
            for(int j = -radius; j <= radius; j++){
                for(int k = -radius; k <= radius; k++){
                    if(j * j + k * k <= radius * radius){
                        if((res_x + j < height) && (res_x + j >=0) && (res_y + k < width) && (res_y + k >= 0))
                            cells[res_x + j][res_y + k].setId(-1);
                    }
                }
            }
        }
    }
    
    public void addSquareInclusions(Graphics g, int numOfInclusions, int diagonal){
        for(int i = 0; i < numOfInclusions; i++){
            Random r = new Random();
            int res_x = r.nextInt(height - 1);
            int res_y = r.nextInt(width - 1);
            if(this.checkForZeros()){
                while(!this.checkGrainBoundaries(cells[res_x][res_y], res_x, res_y) && !this.canAddSquareInclusion(diagonal, res_x, res_y)){
                    res_x = r.nextInt(height - 1);
                    res_y = r.nextInt(width - 1);
                }
            }
            for(int j = 0; j < diagonal; j++){
                for(int k = 0; k < diagonal; k++){
                    if(res_x + j < height && res_y + k < width){
                        cells[res_x + j][res_y + k].setId(-1);
                        DrawComponent.addInclusion(g, res_x + j, res_y + k, cells);
                    }                    
                }
            }
            
        }
    }
    
    public void printCellIds(){
        for(int i = 0; i < this.height; i++){
            for(int j = 0; j < this.width; j++){
                System.out.print(cells[i][j].getId()+ " ");
            }
            System.out.println("");
        }
        System.out.println("");
    }
    
    /**
     * @return the cells
     */
    public Cell[][] getCells() {
        return cells;
    }

    /**
     * @param cells the cells to set
     */
    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return the grainsNum
     */
    public int getGrainsNum() {
        return grainsNum;
    }

    /**
     * @param grainsNum the grainsNum to set
     */
    public void setGrainsNum(int grainsNum) {
        this.grainsNum = grainsNum;
    }
}

