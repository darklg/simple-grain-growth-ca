/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graingrowth;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.Random;
import javax.swing.JPanel;

/**
 *
 * @author darglk
 */
class DrawComponent extends JPanel {
    private static HashMap<Integer, Color> colorId = new HashMap<>();
    
    private static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min)) + min;
        return randomNum;
    }
    
    public DrawComponent(int gridX, int gridY){
        super();
    }
    
    @Override
    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D)g;
        
        Grid grid = new Grid(WIDTH, HEIGHT);
        Cell[][] cells = grid.getCells();
        Cell[][] cellsCopy = new Cell[grid.getHeight()][grid.getWidth()];
        for(int i = 0; i < HEIGHT; i++){
            for(int j = 0; j < WIDTH; j++){
                g2.drawRect(WIDTH + WIDTH * j, HEIGHT + HEIGHT * i, WIDTH, HEIGHT);
                cells[i][j] = new Cell(0, WIDTH + WIDTH * j, HEIGHT + HEIGHT * i, WIDTH, HEIGHT,i,j);
                cellsCopy[i][j] = new Cell(0, WIDTH + WIDTH * j, HEIGHT + HEIGHT * i, WIDTH, HEIGHT,i,j);
            }
        }
        cells[0][0].setId(1);
        cells[7][7].setId(2);
        //cells[4][5] = new InclusionCellSquare(WIDTH + WIDTH * 5, HEIGHT + HEIGHT * 4, WIDTH, HEIGHT, 1);
        grid.addSquareInclusions(g, 5, 1);
        cellsCopy[0][0].setId(1);
        cellsCopy[7][7].setId(2);
        grid.grainGrowth(cellsCopy, g, "neumann", false);
        //grid.grainGrowth(cellsCopy, g, "neumann");
               
    }
    public static void repaintCell(Graphics g, int x, int y, Cell[][] cells, int grainsNum){
        Cell cell = cells[x][y];
        int id= cell.getId();
        if(id > 0){                       
            Color c = colorId.get(id);            
            if(c == null){
                c = new Color(randInt(20,235),randInt(20,235), randInt(20,235));
                colorId.put(id, c);
            }
            g.setColor(c);
            g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());            
                                 
        }else if(id == -1){
            g.setColor(Color.BLACK);
            g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
        }
        g.setColor(Color.BLACK);
    }
    
    public static void repaintCell(Graphics g, int x, int y, Cell[][] cells){
        Cell cell = cells[x][y];
        switch(cell.getId()){ 
            case -1:
                g.setColor(new Color(0, 0,0));
            g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break;
            case 1:
                g.setColor(new Color(0, 255,0));
                g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break;
            case 2:
                g.setColor(new Color(255 , 0 , 0));
                g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break; 
            case 3:
                g.setColor(new Color(0 , 0 , 255));
                g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break;
            case 4:
                g.setColor(Color.CYAN);
                g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break;
            case 5:
                g.setColor(Color.MAGENTA);
                g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break;
            case 6:
                g.setColor(Color.YELLOW);
                g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break;
            case 7:
                g.setColor(Color.ORANGE);
                g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break;
            case 8:
                g.setColor(Color.PINK);
                g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break;
            case 9:
                g.setColor(Color.DARK_GRAY);
                g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break;
            case 10:
                g.setColor(new Color(30, 50, 70));
                g.fillRect(cell.getxCoord(), cell.getyCoord(), cell.getWidth(), cell.getHeight());
                break;
                
        }
        g.setColor(new Color(0 , 0 , 0));
    }
    
    public static void addInclusion(Graphics g, int x, int y, Cell[][] cells){
                  
            g.setColor(Color.black);
            g.fillRect(cells[x][y].getxCoord(), cells[x][y].getyCoord(), cells[x][y].getWidth(), cells[x][y].getHeight());      
    }
    
    public static int WIDTH = 10;
    public static int HEIGHT = 10;
}

